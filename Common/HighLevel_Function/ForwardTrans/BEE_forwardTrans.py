import torch
import torch.nn as nn
import torch.nn.functional as F
from Common.models.BEE_models.layers import (
    AttentionBlock,
    ResidualBlock,
    ResidualBlockWithStride,
    conv3x3,
    conv, 
)
class BEE_ForwardTrans(nn.Module):
    def __init__(self, N=192, M=192, refine=4, init_weights=True, Quantized=False, oldversion=False, **kwargs):
        super().__init__()
        device = 'cpu' if kwargs.get("device") == None else kwargs.get("device")
        self.DeterminismSpeedup = True
        #self.encParams = None
        #self.encSkip = False
        self.g_a = nn.Sequential(
            ResidualBlockWithStride(3, N, stride = 2, device=device),
            ResidualBlock(N, N, device=device),
            ResidualBlockWithStride(N, N, stride = 2, device=device),
            AttentionBlock(N, device=device),
            ResidualBlock(N, N, device=device),
            ResidualBlockWithStride(N, N, stride = 2, device=device),
            ResidualBlock(N, N, device=device),
            conv3x3(N, N, stride = 2, device=device),
        )
        self.h_a = nn.Sequential(
            conv(M, N, stride = 1, kernel_size = 3),
            nn.LeakyReLU(inplace = True),
            conv(N, N, stride = 2, kernel_size = 5),
            nn.LeakyReLU(inplace = True),
            conv(N, N, stride = 2, kernel_size = 5),
        )

    @staticmethod
    def splitFunc(x, func, splitNum, pad, crop):
        _, _, _, w = x.shape
        w_step = ((w + splitNum - 1) // splitNum)
        pp = []
        for i in range(splitNum):
            start_offset = pad if i > 0 else 0
            start_crop = crop if i > 0 else 0
            end_offset = pad if i < (splitNum - 1) else 0
            end_crop = crop if i < (splitNum - 1) else 0
            dummy = func(x[:, :, :, (w_step * i) - start_offset:w_step * (i + 1) + end_offset])
            dummy = dummy[:, :, :, start_crop:]
            if end_crop > 0:
                dummy = dummy[:, :, :, :-end_crop]
            pp.append(dummy)
        x_hat = torch.cat(pp, dim = 3)
        return x_hat

    def update(self,header):
        self.DeterminismSpeedup = header.picture.picture_header.deterministic_processing_flag

    def encode(self, input_variable, header, device):
        imArray = input_variable[0]
        torch.backends.cudnn.deterministic = False if self.DeterminismSpeedup else True
        y = self.splitFunc(imArray, self.g_a, 4, 64, 4)
        z = self.h_a(y)
        return [y, z, input_variable[0], input_variable[1]]
        
    