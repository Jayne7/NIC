import time
import torch
import torch.nn as nn
import torch.nn.functional as F
import einops
import numpy as np
import AE

from Common.models.NIC_models.basic_module import P_NN
from Common.models.NIC_models.context_model import P_Model, Weighted_Gaussian_res_2mask, Weighted_Gaussian
from Common.models.NIC_models.hyper_module import h_synthesisTransform
from Common.models.NIC_models.model import Hyper_Dec


class NIC_Bin2Symbol(nn.Module):
    def __init__(self, header, **kwargs):
        super().__init__()
        device = 'cpu' if kwargs.get("device") == None else kwargs.get("device")
        H, W, CTU_size, model_index = header.H, header.W, header.CTU_size, header.model_index
        if header.USE_VR_MODEL:
            M, N2 = 192, 128
            if (model_index == 2) or (model_index == 5):
                M, N2 = 256, 192
        else:
            M, N2 = 192, 128
            if (model_index == 6) or (model_index == 7) or (model_index == 14) or (model_index == 15):
                M, N2 = 256, 192

        if header.USE_MULTI_HYPER:
            if M == 192:
                self.hyper_1_dec = h_synthesisTransform(256, [768, 768, 768, M], [1, 1, 1])
                self.hyper_2_dec = h_synthesisTransform(128, [64 * 4, 64 * 4, 64 * 4, 64 * 4], [1, 1, 1])
            elif M == 256:
                self.hyper_1_dec = h_synthesisTransform(256, [768 * 2, 768 * 2, 768 * 2, M], [1, 1, 1])
                self.hyper_2_dec = h_synthesisTransform(128, [64 * 4 * 2, 64 * 4 * 2, 64 * 4 * 2, 64 * 4], [1, 1, 1])

        self.p = P_Model(M)

        if header.USE_PREDICTOR:
            self.Y = P_NN(M, 2)
        else:
            self.hyper_dec = Hyper_Dec(N2, M)

        if header.USE_PREDICTOR:
            self.context = Weighted_Gaussian_res_2mask(M)
        else:
            self.context = Weighted_Gaussian(M)

    def Models2device(self,device):
        raise NotImplementedError

    def decode(self, y_hyper_q, header, block_header):
        if header.GPU:
            y_hyper_q = y_hyper_q.cuda()
            if header.USE_MULTI_HYPER:
                tmp = self.hyper_1_dec(y_hyper_q)
                hyper_dec = self.p(tmp)
                if header.USE_PREDICTOR:
                    y_main_predict = self.Y(tmp)
            else:
                hyper_dec = self.p(self.hyper_dec(y_hyper_q))
                print("check3")
            
            precise = 16
            enc_height, enc_width = block_header.enc_height, block_header.enc_width
            Min_Main, Max_Main = block_header.Min_Main, block_header.Max_Main
            
            c_main = block_header.c_main
            EC_this_block = block_header.EC_this_block
            
            h, w = int(enc_height / 16), int(enc_width / 16)
            sample = np.arange(Min_Main, Max_Main + 1 + 1)  # [Min_V - 0.5 , Max_V + 0.5]

            sample = torch.FloatTensor(sample)
            if header.GPU:
                sample = sample.cuda()

            p3d = (5, 5, 5, 5, 5, 5)
            y_main_q = torch.zeros(1, 1, c_main + 10, h + 10, w + 10)  # 8000x4000 -> 500*250
            ## prediction with padding
            y_main_predict_padding = torch.zeros_like(y_main_q)
            y_main_predict_padding[:, :, 5: -5, 5: -5, 5: -5] = y_main_predict.unsqueeze(1)

            if header.GPU:
                y_main_q = y_main_q.cuda()
                y_main_predict_padding = y_main_predict_padding.cuda()
                if header.USE_VR_MODEL:
                    header.lambda_rd = header.lambda_rd.cuda()
            AE.init_decoder("main.bin", Min_Main, Max_Main)
            hyper = torch.unsqueeze(self.context.conv3(hyper_dec), dim=1)
            print("check4")
            self.context.conv1.weight.data *= self.context.conv1.mask
            self.context.conv4.weight.data *= self.context.conv4.mask
            x1_1_on_predict = F.conv3d(y_main_predict_padding,
                                       weight=self.context.conv4.weight,
                                       bias=self.context.conv4.bias)

            for i in range(c_main):
                T = time.time()
                if header.USE_ACCELERATION and EC_this_block[i]==0:
                    y_main_q[0, 0, i + 5, 5: int(enc_height / 16) + 5, 5: int(enc_width / 16) + 5] = y_main_predict[0, i, :int(enc_height / 16), :int(enc_width / 16)]
                    continue
                for j in range(int(enc_height / 16)):
                    for k in range(int(enc_width / 16)):

                        x1 = F.conv3d(y_main_q[:, :, i:i + 12, j:j + 12, k:k + 12],
                                        weight=self.context.conv1.weight, bias=self.context.conv1.bias)  # [1,24,1,1,1]

                        _, _, c_t, h_t, w_t = x1.shape
                        x1_1 = x1_1_on_predict[:, :, i:i + c_t, j:j + h_t, k:k + w_t]
                        x1 = x1 + x1_1
                        params_prob = self.context.conv2(
                            torch.cat((x1, hyper[:, :, i:i + 2, j:j + 2, k:k + 2]), dim=1))

                        # 3 gaussian
                        prob0, mean0, scale0, prob1, mean1, scale1, prob2, mean2, scale2 = params_prob[
                                                                                            0, :, 0, 0, 0]
                        # keep the weight  summation of prob == 1
                        probs = torch.stack([prob0, prob1, prob2], dim=-1)
                        probs = F.softmax(probs, dim=-1)

                        # process the scale value to positive non-zero
                        scale0 = torch.abs(scale0)
                        scale1 = torch.abs(scale1)
                        scale2 = torch.abs(scale2)
                        scale0[scale0 < 1e-6] = 1e-6
                        scale1[scale1 < 1e-6] = 1e-6
                        scale2[scale2 < 1e-6] = 1e-6
                        # 3 gaussian distributions
                        m0 = torch.distributions.normal.Normal(mean0.view(1, 1).repeat(
                            1, Max_Main - Min_Main + 2), scale0.view(1, 1).repeat(1, Max_Main - Min_Main + 2))
                        m1 = torch.distributions.normal.Normal(mean1.view(1, 1).repeat(
                            1, Max_Main - Min_Main + 2), scale1.view(1, 1).repeat(1, Max_Main - Min_Main + 2))
                        m2 = torch.distributions.normal.Normal(mean2.view(1, 1).repeat(
                            1, Max_Main - Min_Main + 2), scale2.view(1, 1).repeat(1, Max_Main - Min_Main + 2))
                        lower0 = m0.cdf(sample - 0.5)
                        lower1 = m1.cdf(sample - 0.5)
                        lower2 = m2.cdf(sample - 0.5)  # [1,c,h,w,Max-Min+2]
                        if header.GPU:
                            lower0 = lower0.cuda()
                            lower1 = lower1.cuda()
                            lower2 = lower2.cuda()

                        lower = probs[0:1] * lower0 + probs[1:2] * lower1 + probs[2:3] * lower2
                        cdf_m = lower.data.cpu().numpy() * ((1 << precise) - (Max_Main -
                                                                                Min_Main + 1))  # [1, c, h, w ,Max-Min+1]
                        cdf_m = cdf_m.astype(np.int) + \
                                sample.cpu().numpy().astype(np.int) - Min_Main

                        pixs = AE.decode_cdf(cdf_m[0, :].tolist())
                        if header.USE_PREDICTOR:
                            y_main_q[0, 0, i + 5, j + 5, k + 5] = pixs + y_main_predict[0, i, j, k]
                        else:
                            y_main_q[0, 0, i + 5, j + 5, k + 5] = pixs
                print("Decoding Channel (%d/192), Time (s): %0.4f" % (i, time.time() - T))
            del hyper, hyper_dec
            quant_latent = y_main_q[0, :, 5:-5, 5:-5, 5:-5]
        return quant_latent 
