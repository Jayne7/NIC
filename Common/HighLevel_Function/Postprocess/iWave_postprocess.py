import torch
import torch.nn as nn
import numpy as np
from PIL import Image

class iWave_Postprocess(nn.Module):
    def __init__(self,  **kwargs):
        super().__init__()
        self.model_post = None
        self.model_postGAN = None

    def decode(self, img_array, header, recon_path):

        height = header.picture.picture_header.vertical_size
        width = header.picture.picture_header.horizontal_size

        with torch.no_grad():
            if header.picture.picture_header.second_step_filtering:
            
                recon = self.model_post(img_array)
            
                if height * width > 1080 * 1920:
                    h_list = [0, height // 2, height]
                    w_list = [0, width // 2, width]
                    k_ = 2
                else:
                    h_list = [0, height]
                    w_list = [0, width]
                    k_ = 1
                gan_rgb_post = torch.zeros_like(recon)
                for _i in range(k_):
                    for _j in range(k_):
                        pad_start_h = max(h_list[_i] - 64, 0) - h_list[_i]
                        pad_end_h = min(h_list[_i + 1] + 64, height) - h_list[_i + 1]
                        pad_start_w = max(w_list[_j] - 64, 0) - w_list[_j]
                        pad_end_w = min(w_list[_j + 1] + 64, width) - w_list[_j + 1]
                        tmp = self.model_postGAN(recon[:, :, h_list[_i] + pad_start_h:h_list[_i + 1] + pad_end_h,
                                                     w_list[_j] + pad_start_w:w_list[_j + 1] + pad_end_w])
                        gan_rgb_post[:, :, h_list[_i]:h_list[_i + 1], w_list[_j]:w_list[_j + 1]] = tmp[:, :,
                                                                                                   -pad_start_h:tmp.size()[
                                                                                                                    2] - pad_end_h,
                                                                                                   -pad_start_w:tmp.size()[
                                                                                                                    3] - pad_end_w]
                recon = gan_rgb_post
            else:
                recon = img_array
                h_list = [0, height // 3, height // 3 * 2, height]
                w_list = [0, width // 3, width // 3 * 2, width]
                k_ = 3
                rgb_post = torch.zeros_like(recon)
                for _i in range(k_):
                    for _j in range(k_):
                        pad_start_h = max(h_list[_i] - 64, 0) - h_list[_i]
                        pad_end_h = min(h_list[_i + 1] + 64, height) - h_list[_i + 1]
                        pad_start_w = max(w_list[_j] - 64, 0) - w_list[_j]
                        pad_end_w = min(w_list[_j + 1] + 64, width) - w_list[_j + 1]
                        tmp = self.model_post(recon[:, :, h_list[_i] + pad_start_h:h_list[_i + 1] + pad_end_h,
                                                  w_list[_j] + pad_start_w:w_list[_j + 1] + pad_end_w])
                        rgb_post[:, :, h_list[_i]:h_list[_i + 1], w_list[_j]:w_list[_j + 1]] = tmp[:, :,
                                                                                               -pad_start_h:tmp.size()[
                                                                                                                2] - pad_end_h,
                                                                                               -pad_start_w:tmp.size()[
                                                                                                                3] - pad_end_w]
                recon = rgb_post

        recon = torch.clamp(torch.round(recon), 0., 255.)
        recon = recon[0, :, :, :]
        recon = recon.permute(1, 2, 0)
        recon = recon.cpu().data.numpy().astype(np.uint8)
        img = Image.fromarray(recon, 'RGB')
        img.save(recon_path)
